-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Agu 2020 pada 17.43
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenge`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hadiah`
--

CREATE TABLE `tbl_hadiah` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `gift_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `poin` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `update_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_hadiah`
--

INSERT INTO `tbl_hadiah` (`id`, `id_user`, `gift_name`, `image`, `poin`, `created_at`, `update_at`) VALUES
(1, 4, 'motor', 'motor.jpg', 1000, '1596535023', '1596535023'),
(2, 4, 'motor', 'motor.jpg', 1000, '1596535093', '1596535093'),
(4, 4, 'mobil', 'mobil.jpg', 10000, '1596535165', '1596536219');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name_product` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` text NOT NULL,
  `update_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_produk`
--

INSERT INTO `tbl_produk` (`id`, `id_user`, `name_product`, `image`, `price`, `created_at`, `update_at`) VALUES
(1, 4, 'jasa buat web', 'Aplikasi-Marketing-Gratis-Terbaik-Leadsius.jpg', 20000, '1596515025', '1596785803'),
(3, 5, 'sendal a', 'sendal.jpg', 10000, '1596515667', '1596515667'),
(34, 4, 'gamis Syar\'i', '26812127maxresdefault.jpg', 10000, '1596709890', '1596709890'),
(36, 4, 'mermaid', '94918932gamisMermaid.jpg', 1300000, '1596710757', '1596786607'),
(37, 4, 'Trend Gamis', '17647944hqdefault.jpg', 60000, '1596784507', '1596786640'),
(38, 4, 'tunik', '30462763ZA3.png', 3000, '1596785842', '1596786347'),
(40, 4, 'sadas', '63818567syari.jpg', 1000, '1596789048', '1596789048'),
(41, 4, 'DAAD', '12452136maxresdefault.jpg', 1000, '1596789097', '1596789097'),
(42, 4, 'sada', '19524198gamisMermaid.jpg', 1000, '1596811219', '1596811219');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `details` varchar(255) NOT NULL,
  `pay` int(11) NOT NULL,
  `created_at` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id`, `id_user`, `details`, `pay`, `created_at`) VALUES
(1, 4, 'kemul', 100000, '1596419995'),
(3, 4, 'sepatu', 100000, '1596508108');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `email`, `username`, `password`) VALUES
(4, 'chus', 'chus@gmail.com', 'chus', '$2y$10$i9aOJ9gnbrtK8UNT.iWBr.N8Qq4v2TmNgj6UTlEwvbEHB2bGUN.N6'),
(5, 'chusfiandi', 'chusfiandi@gmail.com', 'chusfiandi', '9f73e73738d07b9b33f0ffb143bc9c08'),
(6, 'farid', 'farid@gmail.com', 'farid', 'a1d12da42d4302e53d510954344ad164'),
(7, 'ainun', 'ainun@gmail.com', 'ainun', '$2y$10$i9aOJ9gnbrtK8UNT.iWBr.N8Qq4v2TmNgj6UTlEwvbEHB2bGUN.N6');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_hadiah`
--
ALTER TABLE `tbl_hadiah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
