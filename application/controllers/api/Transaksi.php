<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Transaksi extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transaksi_model', 'transaksiM');
    }

    public function transaksi_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $id_user = $is_valid_token['data']->id;
            $transaksi = $this->transaksiM->getTransaksi($id, $id_user);
            if ($transaksi) {
                $this->response([
                    'status' => TRUE,
                    'data' => $transaksi
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // create transaksi
    public function createTransaksi_post()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);

            $this->form_validation->set_rules('id_product', 'id product', 'trim|required|numeric');
            $this->form_validation->set_rules('qty', 'qty', 'trim|required|max_length[10]|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $insert_data = [
                    'id_user' => $is_valid_token['data']->id,
                    'id_product' => $this->input->post('id_product', TRUE),
                    'qty' => $this->input->post('qty', TRUE),
                    'created_at' => date('D F Y')
                ];
                $output = $this->transaksiM->create_transaksi($insert_data);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " transaction successful "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " transaction failed "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}