<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Award extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model', 'awardM');
    }

    public function award_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $id_user = $is_valid_token['data']->id;
            $hadiah = $this->awardM->getaward($id, $id_user);
            if ($hadiah) {
                $this->response([
                    'status' => TRUE,
                    'data' => $hadiah
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // create award
    public function addaward_post()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);
            $this->form_validation->set_rules('gift_name', 'Name gift', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('poin', 'Poin', 'trim|required|max_length[50]|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $nama = $_FILES['image']['name'];
                $nameImage = rand(1000000, 100000000) . $nama;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['upload_path'] = './assets/img';
                $config['file_name'] = $nameImage;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'file failed upload'
                    ]);
                } else {
                    $insert_data = [
                        'id_user' => $is_valid_token['data']->id,
                        'gift_name' => $this->input->post('gift_name', TRUE),
                        'image' => $nameImage,
                        'poin' => $this->input->post('poin', TRUE),
                        'created_at' => time(),
                        'update_at' => time()
                    ];
                    $output = $this->awardM->add_award($insert_data);
                    if ($output > 0 and !empty($output)) {
                        $message =  [
                            'status' => true,
                            'message' => " add give-away successful "
                        ];
                        $this->response($message, REST_Controller::HTTP_OK);
                    } else {
                        $message =  [
                            'status' => false,
                            'message' => " add give-away failed "
                        ];
                        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // Delete Award
    public function deleteaward_delete($id)
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->security->xss_clean($id);

            if (empty($id) and !is_numeric($id)) {
                $this->response(['status' => FALSE, 'message' => "invalid award ID"], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $delete_award = [
                    'id' => $id,
                    'id_user' => $is_valid_token['data']->id,
                ];
                $output = $this->awardM->delete_award($delete_award);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " award deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " award not deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // Put transaksi
    public function updateaward_put()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $this->form_validation->set_rules('gift_name', 'Name Give-away', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('poin', 'poin', 'trim|required|max_length[50]|numeric');
            $this->form_validation->set_rules('id', 'id', 'trim|required|max_length[50]|numeric');
            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $id = $this->input->post('id');
                $id_user = $is_valid_token['data']->id;
                $award = $this->db->get_where('tbl_hadiah', ['id' => $id, 'id_user' => $id_user])->row();
                $jumlah_award = $this->db->get_where('tbl_hadiah', ['id' => $id, 'id_user' => $id_user])->num_rows();
                $oldImage = $award->image;
                if (empty($_FILES)) {
                    if ($jumlah_award != 0) {
                        $id_user = $is_valid_token['data']->id;
                        $data = [
                            "id_user" => $id_user,
                            "gift_name" => $this->input->post("gift_name"),
                            "poin" => $this->input->post("poin"),
                            "update_at" => time()
                        ];
                        $this->db->update('tbl_hadiah', $data, ['id' => $id]);

                        $this->response([
                            'status' => true,
                            'message' => 'Update success'
                        ], 200);
                    }
                } else {
                    $newImage = rand(1000000, 100000000) . $_FILES['image']['name'];
                    $config['upload_path']          = './assets/img';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    // $new_name = time() . $_FILES["userfiles"]['name'];
                    $config['file_name'] = $newImage;
                    // var_dump($newImage);
                    // die;
                    if ($jumlah_award != 0) {
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('image')) {
                            $this->response([
                                'status' => false,
                                'message' => 'File Gagal di Update'
                            ], 404);
                        } else {
                            unlink(FCPATH . "/assets/img/" . $oldImage);
                            $id_user = $is_valid_token['data']->id;
                            $data = [
                                "id_user" => $id_user,
                                "gift_name" => $this->input->post("gift_name"),
                                "poin" => $this->input->post("poin"),
                                "image" => $newImage,
                                "update_at" => time()
                            ];
                            $this->db->update('tbl_hadiah', $data, ['id' => $id]);
                            $this->response([
                                'status' => true,
                                'message' => 'Update success'
                            ], 200);
                        }
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}