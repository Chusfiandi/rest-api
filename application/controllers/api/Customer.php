<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Customer extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('customer_model', 'customerM');
    }

    // tampilkan customer
    public function customer_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $id_user = $is_valid_token['data']->id;
            $customer = $this->customerM->getcustomer($id, $id_user);
            if ($customer) {
                $this->response([
                    'status' => TRUE,
                    'data' => $customer
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // create customer
    public function createcustomer_post()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);

            $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('username', 'username', 'trim|required|min_length[8]');
            $this->form_validation->set_rules('password', 'password', 'trim|required|max_length[125]');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $insert_data = [
                    'id_user' => $is_valid_token['data']->id,
                    'name' => $this->input->post('name', TRUE),
                    'username' => $this->input->post('username', TRUE),
                    'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
                    'created_at' => time()
                ];
                $output = $this->customerM->create_customer($insert_data);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " customer successful "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " customer failed "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // Delete customer
    public function deletecustomer_delete($id)
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $this->load->library('Authorization_Token');

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->security->xss_clean($id);

            if (empty($id) and !is_numeric($id)) {
                $this->response(['status' => FALSE, 'message' => "invalid customer ID"], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $delete_customer = [
                    'id' => $id,
                    'id_user' => $is_valid_token['data']->id,
                ];
                $output = $this->customerM->delete_customer($delete_customer);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " customer deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " customer not deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}