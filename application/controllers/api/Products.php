<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Products extends REST_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('products_model', 'productsM');
    }



    public function produk_get()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->get('id');
            // var_dump($id);
            // die;
            $id_user = $is_valid_token['data']->id;
            $product = $this->productsM->getProduct($id, $id_user);
            if ($product) {
                $this->response([
                    'status' => TRUE,
                    'data' => $product
                ]);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => 'enggak ada'
                ]);
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // create Product
    public function addProduct_post()
    {
        header("Access-Control-Allow-Origin: *");
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $_POST = $this->security->xss_clean($_POST);
            $this->form_validation->set_rules('name_product', 'Name Product', 'trim|required|max_length[50]');
            // $this->form_validation->set_rules('image', 'image', 'trim|required');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[50]|numeric');

            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $nama = $_FILES['image']['name'];
                $nameImage = rand(1000000, 100000000) . $nama;
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['upload_path'] = './assets/img';
                $config['file_name'] = $nameImage;

                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('image')) {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'file failed upload'
                    ]);
                } else {
                    $insert_data = [
                        'id_user' => $is_valid_token['data']->id,
                        'name_product' => $this->input->post('name_product', TRUE),
                        'image' => $nameImage,
                        'price' => $this->input->post('price', TRUE),
                        'created_at' => time(),
                        'update_at' => time()
                    ];
                    $output = $this->productsM->add_products($insert_data);
                    if ($output > 0 and !empty($output)) {
                        $message =  [
                            'status' => true,
                            'message' => " add product successful "
                        ];
                        $this->response($message, REST_Controller::HTTP_OK);
                    } else {
                        $message =  [
                            'status' => false,
                            'message' => " add product failed "
                        ];
                        $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    // Delete Product
    public function deleteProduct_delete($id)
    {
        header("Access-Control-Allow-Origin: *");

        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $id = $this->security->xss_clean($id);

            if (empty($id) and !is_numeric($id)) {
                $this->response(['status' => FALSE, 'message' => "invalid Product ID"], REST_Controller::HTTP_NOT_FOUND);
            } else {
                $delete_product = [
                    'id' => $id,
                    'id_user' => $is_valid_token['data']->id,
                ];
                $output = $this->productsM->delete_product($delete_product);
                if ($output > 0 and !empty($output)) {
                    $message =  [
                        'status' => true,
                        'message' => " Product deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_OK);
                } else {
                    $message =  [
                        'status' => false,
                        'message' => " Product not deleted "
                    ];
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function updateProduct_put()
    {
        header("Access-Control-Allow-Origin: *");
        // token validation
        $is_valid_token = $this->authorization_token->validateToken();
        if (!empty($is_valid_token) and $is_valid_token['status'] === TRUE) {
            $this->form_validation->set_rules('name_product', 'Name Product', 'trim|required|max_length[50]');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[50]|numeric');
            $this->form_validation->set_rules('id', 'id', 'trim|required|max_length[50]|numeric');
            if ($this->form_validation->run() == FALSE) {
                $message =  [
                    'status' => false,
                    'error' => $this->form_validation->error_array(),
                    'message' => validation_errors()
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $id = $this->input->post('id');
                $id_user = $is_valid_token['data']->id;
                $product = $this->db->get_where('tbl_produk', ['id' => $id, 'id_user' => $id_user])->row();
                $jumlah_produk = $this->db->get_where('tbl_produk', ['id' => $id, 'id_user' => $id_user])->num_rows();
                $oldImage = $product->image;
                if (empty($_FILES)) {
                    if ($jumlah_produk != 0) {
                        $id_user = $is_valid_token['data']->id;
                        $data = [
                            "id_user" => $id_user,
                            "name_product" => $this->input->post("name_product"),
                            "price" => $this->input->post("price"),
                            "update_at" => time()
                        ];
                        $this->db->update('tbl_produk', $data, ['id' => $id]);

                        $this->response([
                            'status' => true,
                            'message' => 'Update success'
                        ], 200);
                    }
                } else {
                    $newImage = rand(1000000, 100000000) . $_FILES['image']['name'];
                    $config['upload_path']          = './assets/img';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    // $new_name = time() . $_FILES["userfiles"]['name'];
                    $config['file_name'] = $newImage;
                    // var_dump($newImage);
                    // die;
                    if ($jumlah_produk != 0) {
                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('image')) {
                            $this->response([
                                'status' => false,
                                'message' => 'File Gagal di Update'
                            ], 404);
                        } else {
                            unlink(FCPATH . "/assets/img/" . $oldImage);
                            $id_user = $is_valid_token['data']->id;
                            $data = [
                                "id_user" => $id_user,
                                "name_product" => $this->input->post("name_product"),
                                "price" => $this->input->post("price"),
                                "image" => $newImage,
                                "update_at" => time()
                            ];
                            $this->db->update('tbl_produk', $data, ['id' => $id]);
                            $this->response([
                                'status' => true,
                                'message' => 'Update success'
                            ], 200);
                        }
                    }
                }
            }
        } else {
            $this->response(['status' => FALSE, 'message' => $is_valid_token['message']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
}