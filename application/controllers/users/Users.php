<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Users extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model', 'userM');
    }

    public function register_post()
    {
        header("Access-Control-Allow-Origin: *");

        $_POST = $this->security->xss_clean($_POST);

        $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]|alpha');
        $this->form_validation->set_rules(
            'username',
            'Username',
            'trim|required|max_length[50]|alpha_numeric|is_unique[tbl_user.username]',
            array('is_unique' => 'This %s already exists please enter another username')
        );
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]');
        // $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules(
            'email',
            'Email',
            'trim|required|valid_email|is_unique[tbl_user.email]',
            array('is_unique' => 'This %s already exists please enter another email address')
        );

        if ($this->form_validation->run() == FALSE) {
            $message =  [
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors()
            ];
            $this->response($message, 400);
        } else {
            $insert_data = [
                'name' => $this->input->post('name', TRUE),
                'email' => $this->input->post('email', TRUE),
                'username' => $this->input->post('username', TRUE),
                'password' =>  password_hash($this->input->post('password'), PASSWORD_DEFAULT)
            ];

            $output = $this->userM->insert_user($insert_data);

            if ($output > 0 and !empty($output)) {
                $message =  [
                    'status' => true,
                    'message' => "User registration successful"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message =  [
                    'status' => false,
                    'message' => "Not register your account"
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function fetch_all_users_get()
    {
        $data = $this->users_model->fetch_all_users();
        $this->response($data);
    }

    public function login_post()
    {
        header("Access-Control-Allow-Origin: *");

        $_POST = $this->security->xss_clean($_POST);

        $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[100]');
        // $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');

        if ($this->form_validation->run() == FALSE) {
            $message =  [
                'status' => false,
                'error' => $this->form_validation->error_array(),
                'message' => validation_errors()
            ];
            $this->response($message, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $output = $this->userM->user_login($this->input->post('username'), $this->input->post('password'));
            if (!empty($output) and $output != FALSE) {

                $token_data['id'] = $output->id;
                $token_data['name'] = $output->name;
                $token_data['username'] = $output->username;
                $token_data['email'] = $output->email;
                $token_data['time'] = time();

                $user_token = $this->authorization_token->generateToken($token_data);
                // print_r($this->authorization_token->userData($user_token));
                // exit; 
                $return_data = [
                    'user_id' => $output->id,
                    'name' => $output->name,
                    'username' => $output->username,
                    'email' => $output->email,
                    'token' => $user_token
                ];

                $message =  [
                    'status' => true,
                    'data' => $return_data,
                    'message' => "User login successful"
                ];
                $this->response($message, REST_Controller::HTTP_OK);
            } else {
                $message =  [
                    'status' => false,
                    'message' => "Invalid Username or Password"
                ];
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }
}
