<?php

class Transaksi_model extends CI_model
{
    private $tbltransaksi = "tbl_transaksi";
    public function getTransaksi($id_user, $id)
    {
        $this->db->where('id_user', $id_user);
        if ($id === null) {
            $transaksi = $this->db->get($this->tbltransaksi)->result();
        } else {
            $transaksi = $this->db->get_where($this->tbltransaksi, ['id' => $id])->row();
        }
        return $transaksi;
    }
    public function create_transaksi($insert_data)
    {
        $this->db->insert($this->tbltransaksi, $insert_data);
    }
}