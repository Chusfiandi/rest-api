<?php

class Products_model extends CI_model
{
    protected $products_table = 'tbl_produk';

    public function getProduct($id, $id_user)
    {
        // $data = [
        //     $id_user,
        //     $id
        // ];
        $this->db->where('id_user', $id_user);
        if ($id === null) {
            $product = $this->db->get($this->products_table)->result();
        } else {
            $product = $this->db->get_where($this->products_table, ['id' => $id])->row();
        }
        return $product;
    }

    public function add_products(array $data)
    {
        $this->db->insert($this->products_table, $data);
        return $this->db->insert_id();
    }

    public function delete_product(array $data)
    {
        // var_dump($data);
        // die;
        // cek cocok data dengan id transaksi dan id user
        $query = $this->db->get_where($this->products_table, $data);

        $ambil = $query->row();
        // var_dump($image);
        // die;

        if ($this->db->affected_rows() > 0) {
            $this->db->delete($this->products_table, $data);
            unlink(FCPATH . "/assets/img/" . $ambil->image);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function update_product(array $data)
    {
        // cek cocok data dengan id transaksi dan id user
        $query = $this->db->get_where(
            $this->products_table,
            [
                'id_user' => $data['id_user'],
                'id' => $data['id']
            ]
        );
        // if ($_FILES['image']['name'] != "") {
        //     $config['upload_path']   = './assets/img/';
        //     $config['allowed_types'] = 'gif|jpg|png|jpeg';
        //     $this->load->library('upload', $config);
        // }
        if ($this->db->affected_rows() > 0) {
            $update_data = [
                'name_product' => $data['name_product'],
                'image' => $data['image'],
                'price' => $data['price'],
                'update_at' => time()
            ];
            return $this->db->update($this->products_table, $update_data, ['id' => $query->row('id')]);
        }
        return false;
    }
}
