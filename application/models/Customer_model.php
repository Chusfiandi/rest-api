<?php

class Customer_model extends CI_model
{
    protected $customer_table = 'tbl_customer';

    public function getcustomer($id, $id_user)
    {
        $this->db->where('id_user', $id_user);
        if ($id === null) {
            $customer = $this->db->get($this->customer_table)->result();
        } else {
            $customer = $this->db->get_where($this->customer_table, ['id' => $id])->row();
        }
        return $customer;
    }

    public function create_customer(array $data)
    {
        $this->db->insert($this->customer_table, $data);
        return $this->db->insert_id();
    }

    public function delete_customer(array $data)
    {
        // var_dump($data);
        // die;
        // cek cocok data dengan id customer dan id user
        $query = $this->db->get_where($this->customer_table, $data);
        if ($this->db->affected_rows() > 0) {
            // echo "customer exits";
            // exit;
            $this->db->delete($this->customer_table, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
            return false;
        }
        return false;
    }
}