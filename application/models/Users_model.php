<?php

class Users_model extends CI_model
{
    protected $user_table = 'tbl_user';

    public function insert_user($user_data)
    {
        $this->db->insert($this->user_table, $user_data);
        return $this->db->insert_id();
    }

    public function user_login($username, $password)
    {
        $this->db->where('email', $username);
        $this->db->or_where('username', $username);
        $q = $this->db->get($this->user_table);

        if ($q->num_rows()) {
            $user_pass = $q->row('password');
            if (password_verify($password, $user_pass)) {
                return $q->row();
            }
            return FALSE;
        } else {
            return FALSE;
        }
    }

    // public function fetch_all_users()
    // {
    //     $this->db->select('*')
    //         ->get($user_table);
    //         return $this->db->result;
    // }
}
