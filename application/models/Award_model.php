<?php

class Award_model extends CI_model
{
    protected $awards_table = 'tbl_hadiah';

    public function getaward($id, $id_user)
    {
        $this->db->where('id_user', $id_user);
        if ($id === null) {
            $award = $this->db->get($this->awards_table)->result();
        } else {
            $award = $this->db->get_where($this->awards_table, ['id' => $id])->row();
        }
        return $award;
    }

    public function add_award(array $data)
    {
        $this->db->insert($this->awards_table, $data);
        return $this->db->insert_id();
    }

    public function delete_award(array $data)
    {
        // var_dump($data);
        // die;
        // cek cocok data dengan id transaksi dan id user
        $query = $this->db->get_where($this->awards_table, $data);

        if ($this->db->affected_rows() > 0) {
            // echo "Transaksi exits";
            // exit;
            $this->db->delete($this->awards_table, $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function update_award(array $data)
    {
        // cek cocok data dengan id transaksi dan id user
        $query = $this->db->get_where(
            $this->awards_table,
            [
                'id_user' => $data['id_user'],
                'id' => $data['id']
            ]
        );
        if ($this->db->affected_rows() > 0) {
            $update_data = [
                'gift_name' => $data['gift_name'],
                'image' => $data['image'],
                'poin' => $data['poin'],
                'update_at' => time()
            ];
            return $this->db->update($this->awards_table, $update_data, ['id' => $query->row('id')]);
        }
        return false;
    }
}